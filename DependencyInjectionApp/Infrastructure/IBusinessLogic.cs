﻿namespace Infrastructure
{
    public interface IBusinessLogic
    {
        void ProcessData();
    }
}