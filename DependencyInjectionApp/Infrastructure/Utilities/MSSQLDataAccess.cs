﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Utilities
{
    public class MSSQLDataAccess : IDataAccess
    {
        private readonly ILogger _logger;

        public MSSQLDataAccess(ILogger logger)
        {
            _logger = logger;
        }
        public void LoadData(string database)
        {
            _logger.Log($"Loading data using MSSQL database.");
        }

        public void SaveData(string database)
        {
            _logger.Log($"Saving data using MSSQL database.");
        }
    }
}
