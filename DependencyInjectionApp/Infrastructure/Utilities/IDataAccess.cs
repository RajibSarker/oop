﻿namespace Infrastructure.Utilities
{
    public interface IDataAccess
    {
        void LoadData(string database);
        void SaveData(string database);
    }
}