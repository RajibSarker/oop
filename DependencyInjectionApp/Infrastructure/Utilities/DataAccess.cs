﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Utilities
{
    public class DataAccess : IDataAccess
    {
        public void LoadData(string database)
        {
            Console.WriteLine($"Loading data using: {database} database.");
        }
        public void SaveData(string database)
        {
            Console.WriteLine($"Saving data using: {database} database.");
        }
    }
}
