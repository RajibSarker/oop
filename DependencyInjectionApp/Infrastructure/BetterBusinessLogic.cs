﻿using Infrastructure.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class BetterBusinessLogic : IBusinessLogic
    {
        private readonly ILogger _logger;
        private readonly IDataAccess _dataAccess;

        public BetterBusinessLogic(ILogger logger, IDataAccess dataAccess)
        {
            _logger = logger;
            _dataAccess = dataAccess;
        }
        public void ProcessData()
        {
            _logger.Log("Starting logging using better business logic.");
            Console.WriteLine();
            _dataAccess.SaveData("");
            _dataAccess.LoadData("");
            Console.WriteLine();
            _logger.Log("Completed the data processing.");
        }
    }
}
