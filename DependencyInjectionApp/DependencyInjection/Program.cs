﻿using Autofac;
using DependencyInjection;
using Infrastructure;

//BusinessLogic businessLogic = new BusinessLogic();
//businessLogic.ProcessData();

var container = ContainerConfig.Configure();
using(var scope = container.BeginLifetimeScope())
{
    var app = scope.Resolve<IApplication>();
    app.Run();
}

Console.ReadKey();