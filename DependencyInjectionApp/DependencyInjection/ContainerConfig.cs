﻿using Autofac;
using Infrastructure;
using Infrastructure.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjection
{
    public static class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            //builder.RegisterType<BusinessLogic>().As<IBusinessLogic>();
            builder.RegisterType<BetterBusinessLogic>().As<IBusinessLogic>();
            builder.RegisterType<Application>().As<IApplication>();
            //builder.RegisterType<DataAccess>().As<IDataAccess>();
            builder.RegisterType<Logger>().As<ILogger>();
            builder.RegisterType<MSSQLDataAccess>().As<IDataAccess>();

            //builder.RegisterAssemblyTypes(Assembly.Load(nameof(Infrastructure)))
            //    .Where(c=>c.Namespace.Contains("Utilities"))
            //    .As(c=>c.GetInterfaces().FirstOrDefault(d=>d.Name == "I" + c.Name));

            return builder.Build();
        }
    }
}
